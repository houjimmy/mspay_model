package tw.com.samebest.crm.base;

import java.io.Serializable;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import org.springframework.orm.hibernate5.HibernateTemplate;


import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * 主要 Data Source: 
 * @author suming
 * @param <T> Entity Type
 * @param <Pk> Primary Key Type
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public abstract class GenericBaseDaoImpl<T, Pk extends Serializable> extends AbstractGenericBaseDao<T, Pk> {
	@Autowired
	@Qualifier("annSessionFactory")
	@Override
	protected void setSessionFactory(SessionFactory factory) {
		this.sessionFactory = factory;
	}

	@Autowired
	@Qualifier("hibernateTemplate")
	@Override
	protected void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}

	@Autowired
	@Qualifier("jdbcTemplate")
	@Override
	protected void setJt(JdbcTemplate jt) {
		this.jt = jt;
	}


	/**
	 *
	 */
	@Autowired
	@Qualifier("namedParamJdbcTemplate")
	@Override
	protected void setNpjt(NamedParameterJdbcTemplate npjt) {
		this.npjt = npjt;
	}
}
