package tw.com.samebest.crm.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public abstract class AbstractGenericBaseDao<T, Pk extends Serializable> implements IBaseDao<T, Pk> {

	private Class<T> persistentClass;

	protected SessionFactory sessionFactory;

	protected HibernateTemplate ht;

	protected JdbcTemplate jt;

//	@Deprecated
//	protected SimpleJdbcTemplate sjt;

	protected NamedParameterJdbcTemplate npjt;

	@SuppressWarnings("unchecked")
	public AbstractGenericBaseDao() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	abstract protected void setSessionFactory(SessionFactory factory);

	abstract protected void setHt(HibernateTemplate ht);

	abstract protected void setJt(JdbcTemplate jt);

//	@Deprecated
//	abstract protected void setSjt(SimpleJdbcTemplate sjt);

	abstract protected void setNpjt(NamedParameterJdbcTemplate npjt);

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}


	public List<Map<String, Object>> queryForList(String sql, Map<String, ?> params) {
		return npjt.queryForList(sql, params);
	}


	public Map<String, Object> queryForMap(String strSql, Map<String, Object> mapArgs) {
		Map<String, Object> mapResult = new HashMap<String, Object>();

		try {
			mapResult = npjt.queryForMap(strSql, mapArgs);
		} catch (EmptyResultDataAccessException e) {
		}

		return mapResult;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveOrUpdate(T t) {

		getSession().saveOrUpdate(t);
	}

	@SuppressWarnings("unchecked")
	public T getById(Pk id) {

		return (T) getSession().get(getPersitentClass(), id);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteById(Pk pk) {
		this.getSession().delete(this.getSession().load(persistentClass, pk));
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void save(T entity) {
		getSession().save(entity);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void update(T entity) {
		getSession().update(entity);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(Class<?> clazz, Serializable id) {
		Object obj = getSession().get(clazz, id);
		getSession().delete(obj);
		flush();
	}

	public List<T> findAll() {
		return findByCriteria();
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
		Criteria crit = getSession().createCriteria(getPersitentClass());
		for (Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public T loadById(Pk id) {
		return (T) getSession().load(getPersitentClass(), id);

	}

	public Long selCount() {

		return (Long) getSession().createQuery("select count(*) from " + getClassName()).iterate().next();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveAll(List<T> items) {

		int i = 1;
		Session session = getSession();

		for (T t : items) {
			//session.save(t);
			session.saveOrUpdate(t);

			if (i % 20 == 0) { // 20, same as the JDBC batch size
				// flush a batch of inserts and release memory:
				session.flush();
				session.clear();
			}
			i++;
		}

	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateAll(List<T> items) {

		int i = 1;
		Session session = getSession();

		for (T t : items) {
			//session.save(t);
			session.update(t);

			if (i % 20 == 0) { // 20, same as the JDBC batch size
				// flush a batch of inserts and release memory:
				session.flush();
				session.clear();
			}
			i++;
		}

	}

	public void flush() {
		// getSession().flush();
		ht.flush();
	}

	public String getClassName() {
		return getPersitentClass().getSimpleName();
	}

	protected Class<T> getPersitentClass() {
		return this.persistentClass;
	}
}
