package tw.com.samebest.crm.base;

/**
 * 角色產生器 Interface
 * @author Z00741
 */
public interface IRoleGenerator {

	/**
	 * 取得使用者角色
	 * @param eno 業代
	 * @return
	 */
	public String[] getUserRoles(String eno);
}
