package tw.com.samebest.crm.base;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.orm.hibernate5.HibernateTemplate;

public abstract class AbstractSimpleDao {

	protected SessionFactory sessionFactory;

	protected HibernateTemplate ht;

	protected JdbcTemplate jt;


	protected NamedParameterJdbcTemplate npjt;

	abstract protected void setSessionFactory(SessionFactory factory);

	abstract protected void setHt(HibernateTemplate ht);

	abstract protected void setJt(JdbcTemplate jt);


	abstract protected void setNpjt(NamedParameterJdbcTemplate npjt);

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
