package tw.com.samebest.crm.base;

import java.io.Serializable;
import java.util.List;

/**
 * @author 
 * @param <T>
 * @param <Pk> 
 */
public interface IBaseDao<T, Pk extends Serializable> {

	public T getById(Pk id);

	public T loadById(Pk id);

	public List<T> findAll();

	public void saveOrUpdate(T entity);

	public void saveAll(List<T> entityList);

	public void flush();

	public void deleteById(Pk pk);

	public void save(T entity);

	public void update(T entity);

	public void delete(Class<?> clazz, Serializable id);

	public void updateAll(List<T> items);

	public Long selCount();
}
