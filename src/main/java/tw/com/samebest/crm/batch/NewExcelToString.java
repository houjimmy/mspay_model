package tw.com.samebest.crm.batch;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class NewExcelToString {

	public NewExcelToString() {
		// TODO Auto-generated constructor stub
	}

	
	private static final String FILE_NAME = "/Users/suming/Downloads/10212.xlsx";

	public static void main(String[] args) throws IOException {
	 
		 InputStream ExcelFileToRead = new FileInputStream(FILE_NAME);
	        XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);

	        XSSFSheet sheet = wb.getSheetAt(0);
	        System.out.println("列印名稱："+sheet.getSheetName());
	        
	        
	        XSSFRow row;
	        XSSFCell cell;

	        Iterator rows = sheet.rowIterator();
	        int i =0;
	        while (rows.hasNext()) {
	            row = (XSSFRow) rows.next();
	            Iterator cells = row.cellIterator();
	            
	            while (cells.hasNext()) {
	                cell = (XSSFCell) cells.next();

	                if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
	                    System.out.print(cell.getStringCellValue() + " ");
	                } else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
	                    System.out.print(cell.getNumericCellValue() + " ");
	                } else {
	                    //U Can Handel Boolean, Formula, Errors
	                }
	            }
	            
	            System.out.println("莫名："+i);
	            i++;
	        }
		
	}

}
