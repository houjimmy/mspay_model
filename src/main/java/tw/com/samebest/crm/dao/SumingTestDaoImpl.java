package tw.com.samebest.crm.dao;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import tw.com.samebest.crm.base.GenericBaseDaoImpl;
import tw.com.samebest.crm.model.entity.SumingTest;




@Lazy
@Repository("iSumingTestDao")
public class SumingTestDaoImpl extends GenericBaseDaoImpl<SumingTest, Long> implements ISumingTestDao {
	
	private static final Logger logger = Logger.getLogger(SumingTestDaoImpl.class);
 

}
