package tw.com.samebest.crm.base;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TestTransaction;
//import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext.xml")
public abstract class SpringTXBaseTest {
	
	@Before
	public void setUp(){
		
		System.out.println("test start!");
	}
	
	@After
	public void dearDown(){
		
		System.out.println("test end!");
	}
	
	
	
	
//	@BeforeTransaction
//    public void verifyInitialDatabaseState() {
//        // logic to verify the initial state before a transaction is started
//    }
//
//    @Before
//    public void setUpTestDataWithinTransaction() {
//        // set up test data within the transaction
//    }
//
//    @Test
//    // overrides the class-level defaultRollback setting
//    @Rollback(true)
//    public void modifyDatabaseWithinTransaction() {
//        // logic which uses the test data and modifies database state
//    }
//
//    @After
//    public void tearDownWithinTransaction() {
//        // execute "tear down" logic within the transaction
//    }
//
//    @AfterTransaction
//    public void verifyFinalDatabaseState() {
//        // logic to verify the final state after transaction has rolled back
//    }

   
	
	

}
